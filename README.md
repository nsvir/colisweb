# [ColisWeb](colisweb.com)

A free app to help you grow your business with complete freedom.

This project is an exercise and not the real backend 😉


## How to run
### Backend
- Download the [fatjar](https://gitlab.com/nsvir/colisweb/-/jobs/artifacts/main/download?job=deploy)
- run it
```shell
> unzip artifacts.zip
> HOST=localhost PORT=8080 java -jar target/colisweb-*-jar-with-dependencies.jar
```
### Frontend
- go to [swagger gitlab page](https://nsvir.gitlab.io/colisweb/) to find the documentation

### Run tests
```shell
mvn test
```

## Code

### Guidelines
- Keep the code consistency
- Refactor the code when needed, unit-tests are your friends to ensure you broke nothing
- Code coverage is a dummy number to remind you to not forget your tests. **YOU** are the one to choose what and how things should be tested.
- have fun ✨

### Packages
- Packages are split into business logics: (e.g: transporter, delivery, etc.)
- a business logic is composed by:
  - an `api` package which contains the Payload / Response of the endpoints<br>All data validations `valide()` and conversions from/to a data models are defined in this package
  - a `*Controller` which is the class that handles the http requests / responses<br>It is the bridge between API and the service. It handles validation and service errors
  - a `model` package which represent internal object not exposed to the API<br>All `api` objects are isolated from `model` object. This allows to keep a clean API that is drived by **client needs** and not the inner data model.
  - a `*Service` which is the actual logic of the package
- All classes have its corresponding `test` class. Keep the test at the right level:
  - `Controller`: test errors handling and basic responses
  - `Service`: test the hard business logic with all possible cases
  - `Payload` / `Response`: check json format / conversions / field validation<br>Only put simple cases of field validations because most of the time `WithValidation` is used behind which has its own complete test cases.

## TODO
- improve cask consistency when json parsing failed
- implement database to persist data 
- reduce tests boilerplate
- don't trust [Haversine](https://gitlab.com/colisweb-open-source/scala/scala-distances/-/blob/master/core/src/main/scala/com/colisweb/distances/bird/Haversine.scala) and add some test ([property based test](https://gitlab.com/colisweb-open-source/scala/scala-distances/-/blob/master/core/src/test/scala/com/colisweb/distances/bird/HaversineSpec.scala) seems to be a good idea 😊)

## Discussion
- IMO scope are too tight: `private`
  It is hard to test without code overhead, usually I open scopes to package `private[delivery]` but I wanted to try without.
  Consequences: some methods are not tested enough like: `DeliveryService.isCompatible`
- API / validations seems more tested than the actual business logic. It was a choice in order to have a robust application but I feel that if business logic gets more complex we should invest in business tests
- "Real" end-to-end test with an actual http query is not implemented because so far we can make it work without. I would feel more comfortable if we had at least two http calls (success / failure) on each endpoint but those tests are quite hard to maintain so more expensive. I did not look for a library / tool to use.
