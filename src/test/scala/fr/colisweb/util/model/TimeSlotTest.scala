package fr.colisweb.util.model

import org.scalatest.featurespec.AnyFeatureSpec

class TimeSlotTest extends AnyFeatureSpec {

  Feature("Timeslot validation") {

    Scenario("true") {
      assert(TimeSlot("00:00", "23:59").isRight)
    }

    Scenario("invalid format") {
      assert(TimeSlot("00:00", "invalid").isLeft)
    }

    Scenario("invalid hours") {
      assert(TimeSlot("00:00", "30:00").isLeft)
    }

    Scenario("invalid minutes") {
      assert(TimeSlot("00:00", "00:60").isLeft)
    }

    Scenario("invalid begin greater than end") {
      assert(TimeSlot("23:59", "00:00").isLeft)
    }
  }

  Feature("Timeslot conversion") {
    Scenario("is valid") {
      val actual = TimeSlot("09:30", "17:00")

      val expect = TimeSlot(570, 1020)

      assertResult(Right(expect))(actual)
    }
  }

  Feature("Timeslot isInsideSlot") {
    Scenario("is in slot ith itself") {
      assert(TimeSlot(0, 1).isInsideSlot(TimeSlot(0,1)))
    }

    Scenario("is in slot") {
      assert(TimeSlot(1, 3).isInsideSlot(TimeSlot(0, 4)))
    }

    Scenario("is partially in slot") {
      assertResult(false)(TimeSlot(1, 3).isInsideSlot(TimeSlot(2, 4)))
    }

    Scenario("is not in slot") {
      assertResult(false)(TimeSlot(2, 4).isInsideSlot(TimeSlot(0, 2)))
    }
  }

  Scenario("beginString") {
    assertResult("09:30")(TimeSlot("09:30", "10:30").toOption.get.beginString)
  }

  Scenario("endString") {
    assertResult("10:30")(TimeSlot("09:30", "10:30").toOption.get.endString)
  }

}
