package fr.colisweb.util.model

import org.scalatest.GivenWhenThen
import org.scalatest.featurespec.AnyFeatureSpec

class AreaTest extends AnyFeatureSpec with GivenWhenThen {

  // Average distance between GeoPoint(0, 0) and GeoPoint(1, 1)
  private val defaultGeoPointDistance: DistanceInKm = 160

  Feature("isInsideArea") {
    Scenario("An area is inside itself") {
      val area = Area(GeoPoint(0, 0), defaultGeoPointDistance)

      assert(area.isInsideArea(area))
    }

    Scenario("Same center with different radius") {
      val smaller = Area(GeoPoint(0, 0), defaultGeoPointDistance)
      val bigger = Area(GeoPoint(0, 0), defaultGeoPointDistance * 2)

      assert(smaller.isInsideArea(bigger))
    }

    Scenario("Different center with smaller inside bigger") {
      val smaller = Area(GeoPoint(1, 1), defaultGeoPointDistance)
      val bigger = Area(GeoPoint(0, 0), defaultGeoPointDistance * 2)

      assert(smaller.isInsideArea(bigger))
    }

    Scenario("Different center but smaller is partially inside bigger") {
      val smaller = Area(GeoPoint(1, 1), defaultGeoPointDistance)
      val bigger = Area(GeoPoint(0, 0), defaultGeoPointDistance)

      assert(!smaller.isInsideArea(bigger))
    }

    Scenario("Areas are not intersect") {
      val smaller = Area(GeoPoint(2, 2), defaultGeoPointDistance)
      val bigger = Area(GeoPoint(0, 0), defaultGeoPointDistance)

      assert(!smaller.isInsideArea(bigger))
    }
  }

}
