package fr.colisweb.util

import org.scalatest.featurespec.AnyFeatureSpec

trait JsonTest {
  _: AnyFeatureSpec =>

  private def flattenJson(json: String) = json.replaceAll("\\s", "")

  def assertJson(expect: String, actual: String): Unit = {
    assertResult(flattenJson(expect))(flattenJson(actual))
  }
}
