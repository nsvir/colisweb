package fr.colisweb.util.api

import fr.colisweb.util.model.WeightInKg
import org.scalatest.featurespec.AnyFeatureSpec

/**
 * Used to test validation methods
 */
case class Validation(positiveNumberVal: Double,
                      validGeoPointVal: Double,
                      nonEmptyVal: String,
                      validTimeBeginVal: String,
                      validTimeEndVal: String,
                      singleWeightVal: WeightInKg,
                      totalWeightVal: WeightInKg)
  extends WithValidation {

  override protected val validations: Seq[(Boolean, String)] = Seq(
    validPositiveNumber("positiveNumberVal"),
    validGeoPoint("validGeoPointVal"),
    validNonEmpty("nonEmptyVal"),
    validTime("validTimeBeginVal", "validTimeEndVal"),
    validSingleTotalWeight("singleWeightVal", "totalWeightVal"),
  )
}

class WithValidationTest extends AnyFeatureSpec {

  private val validValidation = Validation(
    positiveNumberVal = 1,
    validGeoPointVal = 180,
    nonEmptyVal = "nonEmpty",
    validTimeBeginVal = "00:00",
    validTimeEndVal = "23:59",
    singleWeightVal = 1,
    totalWeightVal = 2
  )

  private def assertInvalid(validation: Validation, numInvalids: Int = 1) = {
    assertResult(numInvalids)(validation.validate().length)
  }

  private def assertValid(validation: Validation) = assert(validation.validate().isEmpty, validation.validate())

  Scenario("Valid validValidation") {
    assertValid(validValidation)
  }

  Feature("positiveNumber") {
    Scenario("true") {
      assertValid(validValidation.copy(positiveNumberVal = 0))
      assertValid(validValidation.copy(positiveNumberVal = 1))
    }

    Scenario("false") {
      assertInvalid(validValidation.copy(positiveNumberVal = -1))
    }
  }

  Feature("validGeoPoint") {
    Scenario("true") {
      assertValid(validValidation.copy(validGeoPointVal = 0))
      assertValid(validValidation.copy(validGeoPointVal = 180))
      assertValid(validValidation.copy(validGeoPointVal = -180))
    }

    Scenario("false") {
      assertInvalid(validValidation.copy(validGeoPointVal = -181))
      assertInvalid(validValidation.copy(validGeoPointVal = 181))
    }
  }

  Feature("nonEmpty") {
    Scenario("true") {
      assertValid(validValidation.copy(nonEmptyVal = "not empty"))
    }

    Scenario("false") {
      assertInvalid(validValidation.copy(nonEmptyVal = ""))
    }
  }

  Feature("validTime") {
    Scenario("true") {
      assertValid(validValidation.copy(validTimeBeginVal = "00:00", validTimeEndVal = "23:59"))
    }

    Scenario("false") {
      assertInvalid(validValidation.copy(validTimeBeginVal = "23:59", validTimeEndVal = "00:00"))
    }
  }

  Feature("validSingleTotalWeight") {
    Scenario("true") {
      assertValid(validValidation.copy(singleWeightVal = 1, totalWeightVal = 10))
    }

    Scenario("false") {
      assertInvalid(validValidation.copy(singleWeightVal = 10, totalWeightVal = 1))
    }
  }
}
