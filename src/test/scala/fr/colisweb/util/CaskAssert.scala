package fr.colisweb.util

import cask.Response
import cask.endpoints.JsonData
import org.scalatest.Assertions.assertResult

import java.io.ByteArrayOutputStream

trait CaskAssert {

  protected def assertJsonData(expect: String, response: Response[JsonData]): Unit = {
    val stream = new ByteArrayOutputStream()
    response.data.write(stream)
    assertResult(expect)(stream.toString)
  }

}
