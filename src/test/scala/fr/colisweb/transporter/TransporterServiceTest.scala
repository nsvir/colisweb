package fr.colisweb.transporter

import fr.colisweb.transporter.model.Transporter
import org.scalatest.featurespec.AnyFeatureSpec
import org.scalatest.{BeforeAndAfter, GivenWhenThen}

class TransporterServiceTest extends AnyFeatureSpec with GivenWhenThen with BeforeAndAfter {

  before {
    Transporter.clear()
  }

  Scenario("clear") {
    Given("a saved transporter")
    Transporter.save(ValidTransporter)

    When("I clear the transporters")
    Transporter.clear()

    Then("transporters are cleared")
    val expect = Seq.empty[Transporter]
    assertResult(expect)(Transporter.getAll)
  }

  Feature("save / get") {

    Scenario("get nothing") {
      Given("nothing saved")

      When("I get all transporters")
      val actual = Transporter.getAll

      Then("I get en empty list")
      val expect = Seq.empty[Transporter]
      assertResult(expect)(actual)
    }

    Scenario("successful save") {
      When("I save a valid transporter")
      Transporter.save(ValidTransporter)

      Then("the transporter is saved")
      val expect = Seq(ValidTransporter)
      assertResult(expect)(Transporter.getAll)
    }

    Scenario("successful save / get two elements") {
      Given("A saved transport named Anna")
      Transporter.save(ValidTransporter.copy(name = "Anna"))

      When("I save a transporter named John")
      Transporter.save(ValidTransporter.copy(name = "John"))

      Then("I get both transporters")
      val expect = Seq("Anna", "John")
      assertResult(expect)(Transporter.getAll.map(_.name))
    }

    Scenario("invalid save") {
      Given("A saved transport named John")
      Transporter.save(ValidTransporter.copy(name = "John"))

      When("I try to save another John")
      val actual = Transporter.save(ValidTransporter.copy(name = "John"))

      Then("An exception is returned")
      assert(actual.isFailure)
    }
  }

}
