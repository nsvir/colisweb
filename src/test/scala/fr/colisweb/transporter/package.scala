package fr.colisweb

import fr.colisweb.transporter.model.Transporter
import fr.colisweb.util.model.{Area, GeoPoint, TimeSlot}

package object transporter {

  val ValidTransporter: Transporter = Transporter(
    name = "name",
    workArea = Area(GeoPoint(0, 0), 0),
    workTime = TimeSlot("00:00", "23:59").toOption.get,
    totalWeight = 1,
    totalVolume = 1,
    maxItemWeight = 1,
    averageSpeed = 1,
    price = 1
  )

}
