package fr.colisweb.transporter.api

import fr.colisweb.util.JsonTest
import org.scalatest.featurespec.AnyFeatureSpec

/** ensure that the payload validation is activated on columns */
class TransporterPayloadTest extends AnyFeatureSpec with JsonTest {

  private val payload = TransporterPayload(
    name = "Jack",
    workAreaLat = 0.0,
    workAreaLong = 0.0,
    workAreaRadius = 1.0,
    workTimeBegin = "10:00",
    workTimeEnd = "10:30",
    totalWeight = 2.0,
    totalVolume = 1.0,
    maxItemWeight = 1.0,
    averageSpeed = 1.0,
    price = 1.0)

  private val validJson =
    """{
          "name": "Jack",
          "workAreaLat": 0,
          "workAreaLong": 0,
          "workAreaRadius": 1,
          "workTimeBegin": "10:00",
          "workTimeEnd": "10:30",
          "totalWeight": 2,
          "totalVolume": 1,
          "maxItemWeight": 1,
          "averageSpeed": 1,
          "price": 1
      }"""

  Scenario("TransporterPayload can read json") {
    assertResult(payload)(upickle.default.read[TransporterPayload](validJson))
  }

  Scenario("TransporterPayload can write json") {
    assertJson(validJson, upickle.default.write(payload))
  }

  Scenario("toTransporter / apply is equal to itself") {
    assertResult(payload)(TransporterPayload(payload.toTransporter))
  }

  private def assertInvalid(payload: TransporterPayload, fieldName: String, expectedCount: Int = 1) = {
    val invalidFields = payload.validate()
    assertResult(expectedCount, invalidFields)(invalidFields.length)
    val atLeastOneContainsFieldName = invalidFields.map(_.contains(fieldName)).reduce(_ || _)
    assert(atLeastOneContainsFieldName, invalidFields)
  }

  Feature("validate") {

    Scenario("valid TransportPayload") {
      assertResult(Seq.empty)(payload.validate())
    }

    Scenario("invalid totalWeight lower than maxItemWeight") {
      assertInvalid(payload.copy(maxItemWeight = 2, totalWeight = 1), "totalWeight")
    }

    Scenario("invalid empty name") {
      assertInvalid(payload.copy(name = ""), "name")
    }

    Scenario("invalid workAreaLat 181") {
      assertInvalid(payload.copy(workAreaLat = 181), "workAreaLat")
    }

    Scenario("invalid workAreaLong 181") {
      assertInvalid(payload.copy(workAreaLat = 181), "workAreaLat")
    }

    Scenario("invalid workAreaRadius -1") {
      assertInvalid(payload.copy(workAreaRadius = -1), "workAreaRadius")
    }

    Scenario("invalid totalWeight -1") {
      assertInvalid(payload.copy(totalWeight = -1, maxItemWeight = -2), "totalWeight", 2)
    }

    Scenario("invalid totalVolume -1") {
      assertInvalid(payload.copy(totalVolume = -1), "totalVolume")
    }

    Scenario("invalid maxItemWeight -1") {
      assertInvalid(payload.copy(maxItemWeight = -1), "maxItemWeight")
    }

    Scenario("invalid averageSpeed -1") {
      assertInvalid(payload.copy(averageSpeed = -1), "averageSpeed")
    }

    Scenario("invalid price -1") {
      assertInvalid(payload.copy(price = -1), "price")
    }

    Scenario("invalid workTimeBegin / workTimeEnd") {
      assertInvalid(payload.copy(workTimeBegin = "10:00", workTimeEnd = "08:00"), "workTimeBegin")
    }
  }
}
