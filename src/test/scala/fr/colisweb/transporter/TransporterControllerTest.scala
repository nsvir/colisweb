package fr.colisweb.transporter

import fr.colisweb.transporter.api.TransporterPayload
import fr.colisweb.util.{CaskAssert, FeatureSpec}
import org.scalamock.scalatest.MockFactory

import scala.util.{Failure, Success}

class TransporterControllerTest extends FeatureSpec with MockFactory with CaskAssert {

  private val transporterPayload: TransporterPayload = mock[TransporterPayload]
  private val transporterService: TransporterService = mock[TransporterService]

  private val transporterController = TransporterController(transporterService)
  
  Feature("createTransporter") {
    Scenario("200 save success") {
      Given("A valid transporterPayload")
      (transporterPayload.validate _).expects().returns(Seq.empty)
      (transporterPayload.toTransporter _).expects().returns(ValidTransporter)

      When("Save is successful")
      (transporterService.save _).expects(*).returns(Success())

      val actual = transporterController.createTransporter(transporterPayload)

      Then("Status code is 200")
      assertResult(200)(actual.statusCode)
    }

    Scenario("409 save failed") {
      Given("A valid transporterPayload")
      (transporterPayload.validate _).expects().returns(Seq.empty)
      (transporterPayload.toTransporter _).expects().returns(ValidTransporter)

      When("Save failed")
      (transporterService.save _).expects(*).returns(Failure(new RuntimeException("conflict id")))
      val actual = transporterController.createTransporter(transporterPayload)

      Then("Status code is 409 conflict")
      assertResult(409)(actual.statusCode)
      assertJsonData(""""conflict id"""", actual)
    }

    Scenario("400 Bad request") {
      Given("A transporterPayload with invalid fields")
      (transporterPayload.validate _).expects().returns(Seq("Invalid field"))

      val actual = transporterController.createTransporter(transporterPayload)

      Then("Status code is 400")
      assertResult(400)(actual.statusCode)
      assertJsonData("""["Invalid field"]""", actual)
    }
  }

}
