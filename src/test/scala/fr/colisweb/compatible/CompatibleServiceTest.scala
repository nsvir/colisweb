package fr.colisweb.compatible

import fr.colisweb.compatible.model._
import fr.colisweb.transporter.model.Transporter
import fr.colisweb.transporter.{TransporterService, ValidTransporter}
import org.scalamock.scalatest.MockFactory
import org.scalatest.GivenWhenThen
import org.scalatest.featurespec.AnyFeatureSpec

class CompatibleServiceTest extends AnyFeatureSpec with GivenWhenThen with MockFactory {

  private def transporter(deliveryCategory: DeliveryCategory) = {
    ValidTransporter.copy(
      workArea = deliveryCategory.area,
      workTime = deliveryCategory.slot,
      totalWeight = deliveryCategory.totalWeight,
      totalVolume = deliveryCategory.totalVolume,
      maxItemWeight = deliveryCategory.maxItemWeight,
    )
  }

  private val transporterService: TransporterService = mock[TransporterService]

  private val compatibleService = CompatibleService(transporterService)

  Feature("transportersCompatibility : transporters") {

    Scenario("Take all transporter") {
      Given("2 transporters compatible")
      val transportA = transporter(FullCompatibility)
      val transportB = transporter(FullCompatibility)
      (transporterService.getAll _).expects().returning(Seq(transportA, transportB)).once()

      val actual = compatibleService.transportersCompatibility(FullCompatibility)

      Then("Both transporters are returned")
      val expect = Seq(transportA, transportB)
      assertResult(expect.sorted)(actual.map(_.transporter).sorted)
    }

    Scenario("Remove transporter without compatibilities") {
      Given("An incompatible transporter")
      val transportA = transporter(NoCompatibility)
      (transporterService.getAll _).expects().returning(Seq(transportA)).once()

      val actual = compatibleService.transportersCompatibility(FullCompatibility)

      Then("No transporter returned")
      val expect = Seq.empty[Transporter]
      assertResult(expect.sorted)(actual.map(_.transporter).sorted)
    }

    Scenario("Sort by compatibilities") {
      Given("2 transporters with different compatibility")
      val transportA = transporter(FullCompatibility)
      val transportB = transporter(AreaCompatibility)
      (transporterService.getAll _).expects().returning(Seq(transportA, transportB)).once()

      val actual = compatibleService.transportersCompatibility(FullCompatibility)

      Then("transportA is before transportB because more compatible")
      val expect = Seq(transportA, transportB)
      assertResult(expect)(actual.map(_.transporter))
    }
  }

  Scenario("areaCompatibility") {
    val transportA = transporter(AreaCompatibility)
    (transporterService.getAll _).expects().returning(Seq(transportA)).once()

    val actual = compatibleService.transportersCompatibility(FullCompatibility)

    val expect = Seq(TransporterCompatibility(transportA, Seq(AreaCompatible())))
    assertResult(expect)(actual)
  }

  Scenario("timeCompatibility") {
    val transportA = transporter(TimeCompatibility)
    (transporterService.getAll _).expects().returning(Seq(transportA)).once()

    val actual = compatibleService.transportersCompatibility(FullCompatibility)

    val expect = Seq(TransporterCompatibility(transportA, Seq(TimeCompatible())))
    assertResult(expect)(actual)
  }

  Feature("sizeCompatibility") {
    Scenario("all is ok") {
      val transportA = transporter(SizeCompatibility)
      (transporterService.getAll _).expects().returning(Seq(transportA)).once()

      val actual = compatibleService.transportersCompatibility(FullCompatibility)

      val expect = Seq(TransporterCompatibility(transportA, Seq(SizeCompatible())))
      assertResult(expect)(actual)
    }

    Scenario("maxItemWeight is ko") {
      Given("A transporter where maxItemWeight is incompatible")
      val transportA = transporter(SizeCompatibility.copy(maxItemWeight = NoCompatibility.maxItemWeight))
      (transporterService.getAll _).expects().returning(Seq(transportA)).once()

      val actual = compatibleService.transportersCompatibility(FullCompatibility)

      Then("The transporter is not SizeCompatible")
      val expect = Seq.empty[Transporter]
      assertResult(expect)(actual)
    }

    Scenario("totalWeight is ko") {
      Given("A transporter where totalWeight is incompatible")
      val transportA = transporter(SizeCompatibility.copy(totalWeight = NoCompatibility.totalWeight))
      (transporterService.getAll _).expects().returning(Seq(transportA)).once()

      val actual = compatibleService.transportersCompatibility(FullCompatibility)

      Then("The transporter is not SizeCompatible")
      val expect = Seq.empty[Transporter]
      assertResult(expect)(actual)
    }

    Scenario("totalVolume is ko") {
      Given("A transporter where totalVolume is incompatible")
      val transportA = transporter(SizeCompatibility.copy(totalVolume = NoCompatibility.totalVolume))
      (transporterService.getAll _).expects().returning(Seq(transportA)).once()

      val actual = compatibleService.transportersCompatibility(FullCompatibility)

      Then("The transporter is not SizeCompatible")
      val expect = Seq.empty[Transporter]
      assertResult(expect)(actual)
    }
  }
}
