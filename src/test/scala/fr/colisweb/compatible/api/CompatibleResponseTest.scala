package fr.colisweb.compatible.api

import fr.colisweb.compatible.api.CompatibleResponse._
import fr.colisweb.compatible.model.{AreaCompatible, SizeCompatible, TimeCompatible, TransporterCompatibility}
import fr.colisweb.transporter.ValidTransporter
import fr.colisweb.util.JsonTest
import org.scalatest.featurespec.AnyFeatureSpec

class CompatibleResponseTest extends AnyFeatureSpec with JsonTest {

  private val response = CompatibleResponse("name", 1, Seq("compatible"))

  private val validJson =
    """
      {
      		"name": "name",
      		"compatibilityCount": 1,
      		"compatibilities": [
      			"compatible"
      		]
      }
      """

  Scenario("TransporterCompatibilityResponse is valid") {
    assertJson(validJson, upickle.default.write(response))
  }

  Scenario("TransporterCompatibilityResponse from TransporterCompatibility") {
    val actual = CompatibleResponse(TransporterCompatibility(ValidTransporter.copy(name = "name"),
      Seq(AreaCompatible(), SizeCompatible(), TimeCompatible())))
    val expect = CompatibleResponse("name", 3, Seq("AreaCompatible", "SizeCompatible", "TimeCompatible"))
    assertResult(expect)(actual)
  }

}
