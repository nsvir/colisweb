package fr.colisweb.compatible.api

import fr.colisweb.compatible.model.DeliveryCategory
import fr.colisweb.util.JsonTest
import fr.colisweb.util.model.{Area, GeoPoint, TimeSlot}
import org.scalatest.featurespec.AnyFeatureSpec

class CompatiblePayloadTest extends AnyFeatureSpec with JsonTest {
  private val payload = CompatiblePayload(
    workAreaLat = 0.0,
    workAreaLong = 0.0,
    workAreaRadius = 1.0,
    workTimeBegin = "10:00",
    workTimeEnd = "10:30",
    totalWeight = 2.0,
    totalVolume = 1.0,
    maxItemWeight = 1.0)

  private val validJson =
    """{
        "workAreaLat": 0,
        "workAreaLong": 0,
        "workAreaRadius": 1,
        "workTimeBegin": "10:00",
        "workTimeEnd": "10:30",
        "totalWeight": 2,
        "totalVolume": 1,
        "maxItemWeight": 1
      }"""

  private val deliveryCategory = DeliveryCategory(
    Area(GeoPoint(0, 0), 1),
    TimeSlot("10:00", "10:30").toOption.get,
    2.0,
    1.0,
    1.0
  )

  Scenario("DeliveryCategoryPayload is valid") {
    assertResult(payload)(upickle.default.read[CompatiblePayload](validJson))
  }

  Scenario("toDeliveryCategory") {
    assertResult(deliveryCategory)(payload.toDeliveryCategory)
  }

  private def assertInvalid(payload: CompatiblePayload, fieldName: String, expectedCount: Int = 1) = {
    val invalidFields = payload.validate()
    assertResult(expectedCount, invalidFields)(invalidFields.length)
    val atLeastOneContainsFieldName = invalidFields.map(_.contains(fieldName)).reduce(_ || _)
    assert(atLeastOneContainsFieldName, invalidFields)
  }

  Feature("validate") {

    Scenario("valid TransportPayload") {
      assertResult(Seq.empty)(payload.validate())
    }

    Scenario("invalid totalWeight lower than maxItemWeight") {
      assertInvalid(payload.copy(maxItemWeight = 2, totalWeight = 1), "totalWeight")
    }

    Scenario("invalid workAreaLat 181") {
      assertInvalid(payload.copy(workAreaLat = 181), "workAreaLat")
    }

    Scenario("invalid workAreaLong 181") {
      assertInvalid(payload.copy(workAreaLat = 181), "workAreaLat")
    }

    Scenario("invalid workAreaRadius -1") {
      assertInvalid(payload.copy(workAreaRadius = -1), "workAreaRadius")
    }

    Scenario("invalid totalWeight -1") {
      assertInvalid(payload.copy(totalWeight = -1, maxItemWeight = -2), "totalWeight", 2)
    }

    Scenario("invalid totalVolume -1") {
      assertInvalid(payload.copy(totalVolume = -1), "totalVolume")
    }

    Scenario("invalid maxItemWeight -1") {
      assertInvalid(payload.copy(maxItemWeight = -1), "maxItemWeight")
    }
  }
}
