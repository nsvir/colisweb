package fr.colisweb

import fr.colisweb.compatible.model.DeliveryCategory
import fr.colisweb.util.model.{Area, GeoPoint, TimeSlot}

package object compatible {

  val FullCompatibility: DeliveryCategory = DeliveryCategory(
    area = Area(GeoPoint(0, 0), 1),
    slot = TimeSlot("08:00", "10:00").toOption.get,
    totalWeight = 3,
    totalVolume = 3,
    maxItemWeight = 3
  )

  val NoCompatibility: DeliveryCategory = DeliveryCategory(
    area = Area(GeoPoint(1, 1), 0),
    slot = TimeSlot("14:00", "16:00").toOption.get,
    totalWeight = 1,
    totalVolume = 1,
    maxItemWeight = 1
  )

  val AreaCompatibility: DeliveryCategory = DeliveryCategory(
    area = FullCompatibility.area,
    slot = NoCompatibility.slot,
    totalWeight = NoCompatibility.totalWeight,
    totalVolume = NoCompatibility.totalVolume,
    maxItemWeight = NoCompatibility.maxItemWeight
  )

  val TimeCompatibility: DeliveryCategory = DeliveryCategory(
    area = NoCompatibility.area,
    slot = FullCompatibility.slot,
    totalWeight = NoCompatibility.totalWeight,
    totalVolume = NoCompatibility.totalVolume,
    maxItemWeight = NoCompatibility.maxItemWeight
  )

  val SizeCompatibility: DeliveryCategory = DeliveryCategory(
    area = NoCompatibility.area,
    slot = NoCompatibility.slot,
    totalWeight = FullCompatibility.totalWeight,
    totalVolume = FullCompatibility.totalVolume,
    maxItemWeight = FullCompatibility.maxItemWeight
  )


}
