package fr.colisweb.compatible

import fr.colisweb.compatible.api.CompatiblePayload
import fr.colisweb.compatible.model.{AreaCompatible, TransporterCompatibility}
import fr.colisweb.transporter.ValidTransporter
import fr.colisweb.util.{CaskAssert, FeatureSpec}
import org.scalamock.scalatest.MockFactory


class CompatibleControllerTest extends FeatureSpec with MockFactory with CaskAssert {

  private val compatiblePayload: CompatiblePayload = mock[CompatiblePayload]
  private val compatibleService: CompatibleService = mock[CompatibleService]

  private val compatibleController = CompatibleController(compatibleService)


  Feature("getCompatible") {
    Scenario("200 empty list") {
      Given("A compatiblePayload without invalid fields")
      (compatiblePayload.validate _).expects().returns(Seq.empty)
      (compatiblePayload.toDeliveryCategory _).expects().returns(NoCompatibility)

      Given("No compatible transporters")
      (compatibleService.transportersCompatibility _).expects(*).returns(Seq.empty)

      val actual = compatibleController.getCompatible(compatiblePayload)

      Then("Status code is 200")
      assertResult(200)(actual.statusCode)
      assertJsonData("[]", actual)
    }

    Scenario("200 with transporter") {
      Given("A valid compatiblePayload")
      (compatiblePayload.validate _).expects().returns(Seq.empty)
      (compatiblePayload.toDeliveryCategory _).expects().returns(AreaCompatibility)

      Given("An AreaCompatible transporters")
      val areaCompatibleTransporter = TransporterCompatibility(ValidTransporter.copy(name = "name"), Seq(AreaCompatible()))
      (compatibleService.transportersCompatibility _).expects(*).returns(Seq(areaCompatibleTransporter))

      val actual = compatibleController.getCompatible(compatiblePayload)

      Then("Status code is 200")
      assertResult(200)(actual.statusCode)
      assertJsonData("""[{"name":"name","compatibilityCount":1,"compatibilities":["AreaCompatible"]}]""", actual)
    }

    Scenario("400 Bad request") {
      Given("A compatiblePayload with invalid fields")
      (compatiblePayload.validate _).expects().returns(Seq("Invalid field"))

      val actual = compatibleController.getCompatible(compatiblePayload)

      Then("Status code is 400")
      assertResult(400)(actual.statusCode)
      assertJsonData("""["Invalid field"]""", actual)
    }
  }

}
