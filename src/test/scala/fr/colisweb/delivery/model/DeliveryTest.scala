package fr.colisweb.delivery.model

import fr.colisweb.delivery.ValidDelivery
import fr.colisweb.transporter.ValidTransporter
import fr.colisweb.util.FeatureSpec
import fr.colisweb.util.model.{GeoPoint, TimeSlot}

class DeliveryTest extends FeatureSpec {

  // Distance between (0,0) - (1, 1) points is: 157.2 Km
  private val defaultCollectPoint = GeoPoint(0, 0)
  private val defaultDeliveryPoint = GeoPoint(1, 1)

  // At 50 Kmh, we reach 157.2 km in 3h08
  private val defaultSpeed = 50

  private val defaultBeginSlot = "10:00"
  private val onTimeEndSlot = "14:00"
  private val lateEndSlot = "13:00"

  Feature("canMakeInTime") {
    val defaultTransporter = ValidTransporter.copy(averageSpeed = defaultSpeed)
    def defaultSlot(endTime: String) = TimeSlot(defaultBeginSlot, endTime).toOption.get
    val defaultDelivery = ValidDelivery.copy(collect = defaultCollectPoint, delivery = defaultDeliveryPoint)

    Scenario("success case") {
      Given("A delivery where endTime is good enough")
      val delivery = defaultDelivery.copy(slot = defaultSlot(onTimeEndSlot))

      val actual = delivery.canMakeItOnTime(defaultTransporter)

      Then("Default transporter can make it on time")
      assertResult(true)(actual)
    }

    Scenario("failed case") {
      Given("A delivery where endTime is too short")
      val delivery = defaultDelivery.copy(slot = defaultSlot(lateEndSlot))

      val actual = delivery.canMakeItOnTime(defaultTransporter)

      Then("default transporter cannot make it on time")
      assertResult(false)(actual)
    }
  }
}
