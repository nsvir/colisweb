package fr.colisweb.delivery.api

import fr.colisweb.delivery.model.{Delivery, Package}
import fr.colisweb.util.JsonTest
import fr.colisweb.util.model.{GeoPoint, TimeSlot}
import org.scalatest.featurespec.AnyFeatureSpec

/** ensure that the payload validation is activated on columns */
class DeliveryPayloadTest extends AnyFeatureSpec with JsonTest {

  private val payload = DeliveryPayload(
    collectLat = 0.0,
    collectLong = 0.0,
    deliveryLat = 0.0,
    deliveryLong = 0.0,
    slotBegin = "09:00",
    slotEnd = "09:30",
    packages = Seq(PackagePayload(
      weight = 0,
      volume = 0
    )))

  private val delivery: Delivery = Delivery(
    collect = GeoPoint(0, 0),
    delivery = GeoPoint(0, 0),
    slot = TimeSlot("09:00", "09:30").toOption.get,
    packages = Seq(Package(0, 0))
  )

  private val validJson =
    """{
          "collectLat": 0,
          "collectLong": 0,
          "deliveryLat": 0,
          "deliveryLong": 0,
          "slotBegin": "09:00",
          "slotEnd": "09:30",
          "packages": [
            {
              "weight": 0,
              "volume": 0
            }
          ]
        }"""

  Scenario("DeliveryPayload can read json") {
    assertResult(payload)(upickle.default.read[DeliveryPayload](validJson))
  }

  Scenario("toDelivery / apply is equal to itself") {
    assertResult(delivery)(payload.toDelivery)
  }

  private def assertInvalid(payload: DeliveryPayload, fieldName: String, expectedCount: Int = 1) = {
    val invalidFields = payload.validate()
    assertResult(expectedCount, invalidFields)(invalidFields.length)
    val atLeastOneContainsFieldName = invalidFields.map(_.contains(fieldName)).reduce(_ || _)
    assert(atLeastOneContainsFieldName, invalidFields)
  }

  Feature("validate") {

    Scenario("valid TransportPayload") {
      assertResult(Seq.empty)(payload.validate())
    }

    Scenario("invalid collectLat") {
      assertInvalid(payload.copy(collectLat = 181), "collectLat")
    }

    Scenario("invalid collectLong") {
      assertInvalid(payload.copy(collectLong = 181), "collectLong")
    }

    Scenario("invalid deliveryLat") {
      assertInvalid(payload.copy(deliveryLat = 181), "deliveryLat")
    }

    Scenario("invalid deliveryLong") {
      assertInvalid(payload.copy(deliveryLong = 181), "deliveryLong")
    }

    Scenario("invalid slotBegin") {
      assertInvalid(payload.copy(slotBegin = "10:00", slotEnd = "08:00"), "slotBegin")
    }

    Scenario("invalid packages") {
      assertInvalid(payload.copy(packages = Seq.empty), "packages")
    }
  }
}
