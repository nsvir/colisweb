package fr.colisweb.delivery

import fr.colisweb.delivery.model.Delivery
import fr.colisweb.transporter.{TransporterService, ValidTransporter}
import fr.colisweb.util.FeatureSpec
import fr.colisweb.util.model.{Area, GeoPoint, TimeSlot}
import org.scalamock.scalatest.MockFactory

class DeliveryServiceTest extends FeatureSpec with MockFactory {

  private val collectPoint = GeoPoint(0, 0)
  private val deliveryPoint = GeoPoint(0, 0)
  private val compatibleDelivery = ValidDelivery.copy(delivery = deliveryPoint, collect = collectPoint, slot = TimeSlot("10:00", "10:30").toOption.get)
  private val incompatibleDelivery = ValidDelivery.copy(slot = TimeSlot("00:00", "01:00").toOption.get)

  private def transporterBuilder(delivery: Delivery) = {
    ValidTransporter.copy(
      workArea = Area(delivery.collect, 1),
      workTime = delivery.slot,
      totalWeight = delivery.totalWeight,
      totalVolume = delivery.totalVolume,
      maxItemWeight = delivery.maxItemWeight,
    )
  }

  private val transporterService: TransporterService = mock[TransporterService]

  private val deliveryService = DeliveryService(transporterService)

  Feature("bestTransporter") {
    Scenario("Takes the cheapest compatible transporter") {
      Given("Two compatible transporter")
      val cheapTransporter = transporterBuilder(compatibleDelivery).copy(name = "cheap", price = 1)
      val expensiveTransporter = transporterBuilder(compatibleDelivery).copy(name = "expensive", price = 10)
      (transporterService.getAll _).expects().returns(Seq(cheapTransporter, expensiveTransporter))

      val actual = deliveryService.bestTransporter(compatibleDelivery)

      Then("The cheapest is taken")
      val expect = Some(cheapTransporter)
      assertResult(expect)(actual)
    }

    Scenario("Returns none when no transporters compatible") {
      Given("No transporters compatible")
      val incompatibleTransporter = transporterBuilder(incompatibleDelivery)
      (transporterService.getAll _).expects().returns(Seq(incompatibleTransporter))

      val actual = deliveryService.bestTransporter(compatibleDelivery)

      Then("None")
      val expect = None
      assertResult(expect)(actual)
    }
  }

}
