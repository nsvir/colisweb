package fr.colisweb.delivery

import fr.colisweb.delivery.api.DeliveryPayload
import fr.colisweb.transporter.ValidTransporter
import fr.colisweb.util.{CaskAssert, FeatureSpec}
import org.scalamock.scalatest.MockFactory

class DeliveryControllerTest extends FeatureSpec with MockFactory with CaskAssert {

  private val deliveryPayload: DeliveryPayload = mock[DeliveryPayload]
  private val deliveryService: DeliveryService = mock[DeliveryService]

  private val deliveryController = DeliveryController(deliveryService)

  Feature("createDelivery") {
    Scenario("200 transporter found") {
      Given("A compatiblePayload without invalid fields")
      (deliveryPayload.validate _).expects().returns(Seq.empty)
      (deliveryPayload.toDelivery _).expects().returns(ValidDelivery)

      When("best transporter is found")
      (deliveryService.bestTransporter _).expects(*).returns(Some(ValidTransporter.copy(name = "jack")))

      val actual = deliveryController.getDelivery(deliveryPayload)

      Then("200 jack name is returned")
      assertResult(200)(actual.statusCode)
      assertJsonData("""{"name":"jack"}""", actual)
    }

    Scenario("404 not found") {
      Given("A compatiblePayload without invalid fields")
      (deliveryPayload.validate _).expects().returns(Seq.empty)
      (deliveryPayload.toDelivery _).expects().returns(ValidDelivery)

      When("no transporter is found")
      (deliveryService.bestTransporter _).expects(*).returns(None)

      val actual = deliveryController.getDelivery(deliveryPayload)

      Then("204 not found")
      assertResult(404)(actual.statusCode)
    }

    Scenario("invalid payload") {
      Given("An invalid payload")
      (deliveryPayload.validate _).expects().returns(Seq("invalid field"))

      val actual = deliveryController.getDelivery(deliveryPayload)

      Then("400 bad request")
      assertResult(400)(actual.statusCode)
      assertJsonData("""["invalid field"]""", actual)
    }
  }

}
