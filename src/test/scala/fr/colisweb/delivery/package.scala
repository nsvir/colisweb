package fr.colisweb

import fr.colisweb.delivery.model.{Delivery, Package}
import fr.colisweb.util.model.{GeoPoint, TimeSlot}

package object delivery {

  val ValidDelivery: Delivery = Delivery(
    collect = GeoPoint(0, 0),
    delivery = GeoPoint(0, 0),
    slot = TimeSlot("09:00", "09:30").toOption.get,
    packages = Seq(Package(0, 0))
  )
}
