package fr.colisweb.transporter

import fr.colisweb.transporter.model.Transporter

import scala.util.Try

case class TransporterService() {

  /**
   * try to save the given transport
   * @return Failure when the saving process failed
   */
  def save(transporter: Transporter): Try[Unit] = {
    Transporter.save(transporter)
  }

  /**
   * @return All the transporter saved, cannot fail
   */
  def getAll: Seq[Transporter] = {
    Transporter.getAll
  }

}
