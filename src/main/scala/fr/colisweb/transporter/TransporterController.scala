package fr.colisweb.transporter

import cask.Response
import cask.endpoints.JsonData
import cask.model.Status.{BadRequest, Conflict}
import fr.colisweb.transporter.api.TransporterPayload
import fr.colisweb.util.api.Controller

//noinspection AccessorLikeMethodIsEmptyParen
case class TransporterController(transporterService: TransporterService = TransporterService()) extends Controller {

  @cask.get("/transporter")
  def getTransporters(): Response[JsonData] = {
    Response(transporterService.getAll.map(TransporterPayload(_)))
  }

  @cask.postJson("/transporter")
  def createTransporter(transporter: TransporterPayload): Response[JsonData] = {
    val invalidFields = transporter.validate()
    if (invalidFields.isEmpty) {
      val triedSave = transporterService.save(transporter.toTransporter)
      if (triedSave.isFailure) {
        Response(triedSave.failed.get.getMessage, Conflict.code)
      } else {
        Response(transporter)
      }
    } else {
      Response(invalidFields, BadRequest.code)
    }
  }

  initialize()

}
