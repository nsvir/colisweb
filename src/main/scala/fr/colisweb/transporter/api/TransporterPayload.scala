package fr.colisweb.transporter.api

import fr.colisweb.transporter.model
import fr.colisweb.transporter.model.Transporter
import fr.colisweb.util.api.WithValidation
import fr.colisweb.util.model._
import upickle.default.{ReadWriter, macroRW}


/** Payload representation to create a Transporter */
case class TransporterPayload(name: String,
                              workAreaLat: Latitude,
                              workAreaLong: Longitude,
                              workAreaRadius: DistanceInKm,
                              workTimeBegin: String,
                              workTimeEnd: String,
                              totalWeight: WeightInKg,
                              totalVolume: VolumeInCmCube,
                              maxItemWeight: WeightInKg,
                              averageSpeed: SpeedInKmH,
                              price: PriceInEuro) extends WithValidation {

  /**
   * defines the validations required by the instance
   */
  override protected lazy val validations: Seq[(Boolean, String)] = Seq(
    validNonEmpty("name"),
    validPositiveNumber("totalWeight"),
    validPositiveNumber("totalVolume"),
    validPositiveNumber("maxItemWeight"),
    validGeoPoint("workAreaLat"),
    validGeoPoint("workAreaLong"),
    validPositiveNumber("workAreaRadius"),
    validTime("workTimeBegin", "workTimeEnd"),
    validSingleTotalWeight("maxItemWeight", "totalWeight"),
    validPositiveNumber("averageSpeed"),
    validPositiveNumber("price")
  )

  /**
   * convert a transportPayload into the Transporter model
   * Transporter should be validated b
   */
  def toTransporter: Transporter = {
    model.Transporter(
      this.name,
      Area(GeoPoint(this.workAreaLat, this.workAreaLong), this.workAreaRadius),
      TimeSlot(this.workTimeBegin, this.workTimeEnd).toOption.get,
      this.totalWeight,
      this.totalVolume,
      this.maxItemWeight,
      this.averageSpeed,
      this.price
    )
  }

}

object TransporterPayload {
  /** implicit used to read / write the class into a json */
  implicit val rw: ReadWriter[TransporterPayload] = macroRW

  /** convert a Transporter model into the TransporterPayload */
  def apply(transporter: Transporter): TransporterPayload = {
    new TransporterPayload(
      name = transporter.name,
      workAreaLat = transporter.workArea.center.latitude,
      workAreaLong = transporter.workArea.center.longitude,
      workAreaRadius = transporter.workArea.radius,
      workTimeBegin = transporter.workTime.beginString,
      workTimeEnd = transporter.workTime.endString,
      totalWeight = transporter.totalWeight,
      totalVolume = transporter.totalVolume,
      maxItemWeight = transporter.maxItemWeight,
      averageSpeed = transporter.averageSpeed,
      price = transporter.price
    )
  }
}