package fr.colisweb.transporter.model

import fr.colisweb.util.model._

import scala.collection.mutable.ListBuffer
import scala.util.{Failure, Success, Try}

case class Transporter(name: String,
                       workArea: Area,
                       workTime: TimeSlot,
                       totalWeight: WeightInKg,
                       totalVolume: VolumeInCmCube,
                       maxItemWeight: WeightInKg,
                       averageSpeed: SpeedInKmH,
                       price: PriceInEuro)

object Transporter {

  implicit val sort: Ordering[Transporter] = (x: Transporter, y: Transporter) => x.name.compareTo(y.name)

  /**
   * transporter saved in-memory
   */
  private val transporters: ListBuffer[Transporter] = ListBuffer()

  /**
   * @param transporter to save
   * @return Failure if the transporter name is not unique
   */
  private[transporter] def save(transporter: Transporter): Try[Unit] = {
    val nameAlreadyExists = transporters.exists(_.name == transporter.name)
    if (nameAlreadyExists) {
      Failure(new IllegalArgumentException(s"Name ${transporter.name} already exists"))
    } else {
      Success(transporters.addOne(transporter))
    }
  }

  /**
   * @return all the transporters saved
   */
  private[transporter] def getAll: Seq[Transporter] = {
    transporters.toSeq
  }

  /**
   * clear all saved transporters
   */
  private[transporter] def clear(): Unit = {
    transporters.clear()
  }
}