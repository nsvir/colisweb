package fr.colisweb.compatible.api

import fr.colisweb.compatible.model.TransporterCompatibility
import upickle.default.{Writer, macroW}

case class CompatibleResponse(name: String,
                              compatibilityCount: Int,
                              compatibilities: Seq[String])

object CompatibleResponse {
  /** implicit used to read / write the class into a json */
  implicit val rw: Writer[CompatibleResponse] = macroW

  def apply(compatibility: TransporterCompatibility): CompatibleResponse = {
    CompatibleResponse(
      compatibility.transporter.name,
      compatibility.compatibilities.size,
      compatibility.compatibilities.map(_.toString)
    )
  }
}