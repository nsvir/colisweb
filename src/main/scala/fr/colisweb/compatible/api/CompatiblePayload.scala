package fr.colisweb.compatible.api

import fr.colisweb.compatible.model
import fr.colisweb.compatible.model.DeliveryCategory
import fr.colisweb.util.api.WithValidation
import fr.colisweb.util.model._
import upickle.default.{Reader, macroR}

case class CompatiblePayload(workAreaLat: Latitude,
                             workAreaLong: Longitude,
                             workAreaRadius: DistanceInKm,
                             workTimeBegin: String,
                             workTimeEnd: String,
                             totalWeight: WeightInKg,
                             totalVolume: VolumeInCmCube,
                             maxItemWeight: WeightInKg) extends WithValidation {
  /**
   * defines the validations required by the instance
   */
  override protected lazy val validations: Seq[(Boolean, String)] = Seq(
    validPositiveNumber("totalWeight"),
    validPositiveNumber("totalVolume"),
    validPositiveNumber("maxItemWeight"),
    validPositiveNumber("workAreaRadius"),
    validGeoPoint("workAreaLat"),
    validGeoPoint("workAreaLong"),
    validTime("workTimeBegin", "workTimeEnd"),
    validSingleTotalWeight("maxItemWeight", "totalWeight")
  )

  def toDeliveryCategory: DeliveryCategory = {
    model.DeliveryCategory(
      Area(GeoPoint(this.workAreaLat, this.workAreaLong), this.workAreaRadius),
      TimeSlot(this.workTimeBegin, this.workTimeEnd).toOption.get,
      this.totalWeight,
      this.totalVolume,
      this.maxItemWeight
    )
  }
}

object CompatiblePayload {
  /** implicit used to read / write the class into a json */
  implicit val rw: Reader[CompatiblePayload] = macroR
}

