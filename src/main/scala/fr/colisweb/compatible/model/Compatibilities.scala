package fr.colisweb.compatible.model

sealed class Compatibility(value: String) {
  override def toString: String = value
}

case class AreaCompatible() extends Compatibility("AreaCompatible")
case class TimeCompatible() extends Compatibility("TimeCompatible")
case class SizeCompatible() extends Compatibility("SizeCompatible")
