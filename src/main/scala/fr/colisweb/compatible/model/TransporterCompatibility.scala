package fr.colisweb.compatible.model

import fr.colisweb.transporter.model.Transporter

case class TransporterCompatibility(transporter: Transporter, compatibilities: Seq[Compatibility])
