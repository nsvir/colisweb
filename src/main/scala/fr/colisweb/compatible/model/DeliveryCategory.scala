package fr.colisweb.compatible.model

import fr.colisweb.util.model.{Area, TimeSlot, VolumeInCmCube, WeightInKg}

case class DeliveryCategory(area: Area,
                            slot: TimeSlot,
                            totalWeight: WeightInKg,
                            totalVolume: VolumeInCmCube,
                            maxItemWeight: WeightInKg)

