package fr.colisweb.compatible

import fr.colisweb.compatible.model._
import fr.colisweb.transporter.TransporterService
import fr.colisweb.transporter.model.Transporter

case class CompatibleService(transporterService: TransporterService = TransporterService()) {

  /**
   * Test compatibility of all transporter with the given deliveryCategory.
   *
   * There are different types of compatibilities.
   * @see fr.colisweb.compatibility.model.Compatibilities
   *
   * @param deliveryCategory the deliveryCategory to compare the transporter to
   * @return all the transport list with the corresponding compatibilities
   */
  def transportersCompatibility(deliveryCategory: DeliveryCategory): Seq[TransporterCompatibility] = {

    val transporterCompatibilities = transporterService.getAll.map { transporter =>

      val compatibilities: Seq[Compatibility] = Seq(
        areaCompatibility(transporter, deliveryCategory),
        timeCompatibility(transporter, deliveryCategory),
        sizeCompatibility(transporter, deliveryCategory)
      ).flatten

      TransporterCompatibility(transporter, compatibilities)
    }

    transporterCompatibilities
      .filter(_.compatibilities.nonEmpty)
      .sortBy(_.compatibilities.length)
      .reverse
  }

  def areaCompatibility(transporter: Transporter, deliveryCategory: DeliveryCategory): Option[Compatibility] = {
    if (deliveryCategory.area.isInsideArea(transporter.workArea)) {
      Some(AreaCompatible())
    } else None
  }

  def timeCompatibility(transporter: Transporter, deliveryCategory: DeliveryCategory): Option[Compatibility] = {
    if (deliveryCategory.slot.isInsideSlot(transporter.workTime)) {
      Some(TimeCompatible())
    } else None
  }

  def sizeCompatibility(transporter: Transporter, deliveryCategory: DeliveryCategory): Option[Compatibility] = {
    if (deliveryCategory.maxItemWeight <= transporter.maxItemWeight
      && deliveryCategory.totalWeight <= transporter.totalWeight
      && deliveryCategory.totalVolume <= transporter.totalVolume) {

      Some(SizeCompatible())
    } else None
  }
}
