package fr.colisweb.compatible

import cask.Response
import cask.endpoints.JsonData
import cask.model.Status.BadRequest
import fr.colisweb.compatible.api.{CompatiblePayload, CompatibleResponse}
import fr.colisweb.util.api.{Controller, GetJsonPayload}

case class CompatibleController(compatibleService: CompatibleService = CompatibleService()) extends Controller {

  @GetJsonPayload("/compatible")
  def getCompatible(deliveryCategory: CompatiblePayload): Response[JsonData] = {
    val invalidFields = deliveryCategory.validate()
    if (invalidFields.isEmpty) {

      val transportCompatibilities = compatibleService.transportersCompatibility(deliveryCategory.toDeliveryCategory)

      Response(transportCompatibilities.map(CompatibleResponse(_)))
    } else {
      Response(invalidFields, BadRequest.code)
    }
  }

  initialize()
}