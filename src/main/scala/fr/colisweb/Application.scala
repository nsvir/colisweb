package fr.colisweb

import fr.colisweb.compatible.CompatibleController
import fr.colisweb.delivery.DeliveryController
import fr.colisweb.transporter.TransporterController
import io.undertow.server.{HttpHandler, HttpServerExchange}
import io.undertow.server.handlers.BlockingHandler
import io.undertow.util.HttpString

/**
 * Main application that start Cask with the proper controllers
 */
object Application extends cask.Main {

  override def defaultHandler: BlockingHandler = {
    new BlockingHandler((exchange: HttpServerExchange) => {
      exchange.getResponseHeaders.put(new HttpString("Access-Control-Allow-Origin"), "*")
      super.defaultHandler.getHandler.handleRequest(exchange)
    })
  }

  override def port: Int = sys.env.getOrElse("PORT", "8080").toInt

  override def host: String = sys.env.getOrElse("HOST", "localhost")

  println(s"Starting cask on: HOST=${host} PORT=${port}")

  val allRoutes = Seq(
    TransporterController(),
    CompatibleController(),
    DeliveryController()
  )
}