package fr.colisweb.util.model

import scala.util.Try
import scala.util.matching.Regex

case class TimeSlot(begin: MinutesFromMidnight, end: MinutesFromMidnight) {


  /**
   * check that the instance is part of / equal to the other TimeSlot
   *
   * @param other the TimeSlot to check that the instance is inside
   * @return true if the instance is inside / equal the other TimeSlot, false otherwise
   */
  def isInsideSlot(other: TimeSlot): Boolean = {
    other.begin <= this.begin && other.end >= this.end
  }

  private def minutesFromMidnightToString(time: MinutesFromMidnight) = {
    s"%02d:%02d".format(time / 60, time % 60)
  }

  def beginString: String = minutesFromMidnightToString(begin)

  def endString: String = minutesFromMidnightToString(end)

  def duration: DurationInMinutes = end - begin
}

object TimeSlot {

  val TimeSlotFormat: Regex = "([0-9]{2}):([0-9]{2})".r

  /**
   * try to build a Timeslot from the begin / end string
   *
   * @return Left when failed with a description of the error, right with the timeslot object otherwise
   */
  def apply(begin: String, end: String): Either[String, TimeSlot] = {

    for {
      beginMin <- stringToMinutesFromMidnight(begin)
      endMin <- stringToMinutesFromMidnight(end)
      timeSlot <- timeSlotValidation(beginMin, endMin)
    } yield timeSlot
  }

  private def getAndValidateTime(valueStr: String, min: Int, max: Int): Either[String, MinutesFromMidnight] = {
    Try(valueStr.toInt).toEither.left.map(_.getMessage).flatMap { value =>
      if (min <= value && value <= max)
        Right(value)
      else Left(s"$valueStr is not between $min - $max")
    }
  }

  private def stringToMinutesFromMidnight(time: String): Either[String, MinutesFromMidnight] = {
    time match {
      case TimeSlotFormat(hoursStr, minutesStr) =>
        for {
          hours <- getAndValidateTime(hoursStr, 0, 23)
          minutes <- getAndValidateTime(minutesStr, 0, 59)
        } yield hours * 60 + minutes
      case _ => Left(s"$time format is wrong and should be formed like hh:mm (e.g 10:30)")
    }
  }

  private def timeSlotValidation(beginMin: MinutesFromMidnight, endMin: MinutesFromMidnight): Either[String, TimeSlot] = {
    if (beginMin <= endMin) Right(TimeSlot(beginMin, endMin)) else Left("Timeslot error: end should be greater than begin")
  }


}