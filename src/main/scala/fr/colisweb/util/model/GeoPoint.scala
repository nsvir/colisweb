package fr.colisweb.util.model

case class GeoPoint(latitude: Latitude, longitude: Longitude) {

  def toRadians: GeoPoint = GeoPoint(math.toRadians(latitude), math.toRadians(longitude))

}
