package fr.colisweb.util

package object model {

  type SpeedInKmH = Double
  type WeightInKg = Double
  type VolumeInCmCube = Double
  type PriceInEuro = Double

  type DistanceInKm = Double
  type DurationInMinutes = Long

  type Latitude = Double
  type Longitude = Double

  type MinutesFromMidnight = Int

}
