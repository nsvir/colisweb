package fr.colisweb.util.model

import fr.colisweb.util.Haversine

case class Area(center: GeoPoint, radius: DistanceInKm) {


  /**
   * check that the instance is part of / equal to the other Area
   *
   * @param other the area to check that the instance is inside
   * @return true if the instance is inside / equal the other area, false otherwise
   */
  def isInsideArea(other: Area): Boolean = {
    val centersDistance = Haversine.distanceInKm(this.center, other.center)

    centersDistance + this.radius <= other.radius
  }

  /**
   * @param target to check if is inside area
   * @return true if the position is within the instance area
   */
  def contains(target: GeoPoint) = Haversine.distanceInKm(center, target) <= radius

}
