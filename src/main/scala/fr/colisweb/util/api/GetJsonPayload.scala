package fr.colisweb.util.api

/** Like postJson of cask but with the method get */
class GetJsonPayload(value: String) extends cask.endpoints.postJson(value) {
  override val methods = Seq("get")
}
