package fr.colisweb.util.api

import fr.colisweb.util.model.{TimeSlot, WeightInKg}

/** Class used to validate the data of a case class */
trait WithValidation {

  /**
   * defines the validations required by the instance
   */
  protected val validations: Seq[(Boolean, String)]

  /**
   * @return the errors of the instance
   */
  def validate(): Seq[String] = {
    validations.collect { case (false, error) => error }
  }

  private def getField[V](col: String): V = {
    this.getClass.getMethods.find(_.getName == col).get.invoke(this).asInstanceOf[V]
  }

  /** ensure num is >= 0 */
  def validPositiveNumber(col: String): (Boolean, String) = {
    val num: Double = this.getField(col)
    (num >= 0, s"$col must be greater than 0")
  }

  /** ensure geoPoint is between -180 / 180 */
  def validGeoPoint(col: String): (Boolean, String) = {
    val point: Double = this.getField(col)
    (point >= -180 && point <= 180, s"$col must be between -180 and 180")
  }

  /** ensure string is not empty */
  def validNonEmpty(col: String): (Boolean, String) = {
    val str: String = this.getField(col)
    (str.nonEmpty, s"$col must not be empty")
  }

  /** ensure that time has valid formats and that beginCol is lower than the endCol */
  def validTime(beginCol: String, endCol: String): (Boolean, String) = {
    val begin: String = this.getField(beginCol)
    val end: String = this.getField(endCol)

    val timeSlot = TimeSlot(begin, end)
    if (timeSlot.isLeft) {
      (false, s"$beginCol / $endCol are invalid: ${timeSlot.left.getOrElse("")}")
    } else (true, "")
  }

  /** ensure that total weight is greater than single weight */
  def validSingleTotalWeight(singleColumn: String, totalColumn: String): (Boolean, String) = {
    val single: WeightInKg = this.getField(singleColumn)
    val total: WeightInKg = this.getField(totalColumn)
    (single <= total) -> s"$singleColumn cannot be greater than $totalColumn"
  }
}