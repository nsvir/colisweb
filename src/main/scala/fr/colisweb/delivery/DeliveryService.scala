package fr.colisweb.delivery

import fr.colisweb.delivery.model.Delivery
import fr.colisweb.transporter.TransporterService
import fr.colisweb.transporter.model.Transporter

case class DeliveryService(transporterService: TransporterService = TransporterService()) {

  /**
   * find the best transporter that is compatible and the cheapest
   */
  def bestTransporter(delivery: Delivery): Option[Transporter] = {
    transporterService.getAll
      .filter(isCompatible(delivery))
      .minByOption(_.price)
  }

  private def isCompatible(delivery: Delivery)(transporter: Transporter): Boolean = {
    val packagesCompatible = delivery.totalWeight <= transporter.totalWeight &&
      delivery.totalVolume <= transporter.totalVolume &&
      delivery.maxItemWeight <= transporter.maxItemWeight

    val areaCompatible = transporter.workArea.contains(delivery.collect) &&
      transporter.workArea.contains(delivery.delivery)

    val timeCompatible = delivery.slot.isInsideSlot(transporter.workTime)

    val canMakeDeliveryInTime = delivery.canMakeItOnTime(transporter)

    packagesCompatible && areaCompatible && timeCompatible && canMakeDeliveryInTime
  }
}
