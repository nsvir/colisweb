package fr.colisweb.delivery.api

import fr.colisweb.delivery.model.Package
import fr.colisweb.util.api.WithValidation
import fr.colisweb.util.model.{VolumeInCmCube, WeightInKg}
import upickle.default.{Reader, macroR}

case class PackagePayload(weight: WeightInKg,
                          volume: VolumeInCmCube) extends WithValidation {

  override val validations: Seq[(Boolean, String)] = Seq(
    validPositiveNumber("weight"),
    validPositiveNumber("volume"),
  )

  def toPackage: Package = Package(weight, volume)
}

object PackagePayload {
  /** implicit used to read / write the class into a json */
  implicit val rw: Reader[PackagePayload] = macroR
}
