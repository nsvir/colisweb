package fr.colisweb.delivery.api

import fr.colisweb.delivery.model.Delivery
import fr.colisweb.util.api.WithValidation
import fr.colisweb.util.model._
import upickle.default.{Reader, macroR}

case class DeliveryPayload(collectLat: Latitude,
                           collectLong: Longitude,
                           deliveryLat: Latitude,
                           deliveryLong: Longitude,
                           slotBegin: String,
                           slotEnd: String,
                           packages: Seq[PackagePayload]
                          ) extends WithValidation {

  /**
   * defines the validations required by the instance
   */
  override protected lazy val validations: Seq[(Boolean, String)] = Seq(
    validGeoPoint("collectLat"),
    validGeoPoint("collectLong"),
    validGeoPoint("deliveryLat"),
    validGeoPoint("deliveryLong"),
    validTime("slotBegin", "slotEnd"),
    (this.packages.nonEmpty, "packages should not be empty")
  )

  /**
   * @return the errors of the instance and the inner sequence
   */
  override def validate(): Seq[String] = super.validate() ++ packages.flatMap(_.validate())

  def toDelivery: Delivery = {
    Delivery(
      collect = GeoPoint(collectLat, collectLong),
      delivery = GeoPoint(deliveryLat, deliveryLong),
      slot = TimeSlot(slotBegin, slotEnd).toOption.get,
      packages = packages.map(_.toPackage)
    )
  }

}

object DeliveryPayload {
  /** implicit used to read / write the class into a json */
  implicit val rw: Reader[DeliveryPayload] = macroR
}

