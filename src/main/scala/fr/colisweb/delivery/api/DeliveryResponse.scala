package fr.colisweb.delivery.api

import fr.colisweb.transporter.model.Transporter
import upickle.default.{Writer, macroW}

case class DeliveryResponse(name: String)

object DeliveryResponse {
  /** implicit used to read / write the class into a json */
  implicit val rw: Writer[DeliveryResponse] = macroW

  def apply(transporter: Transporter): DeliveryResponse = DeliveryResponse(transporter.name)
}