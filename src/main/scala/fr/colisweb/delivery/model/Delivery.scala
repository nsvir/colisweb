package fr.colisweb.delivery.model

import fr.colisweb.transporter.model.Transporter
import fr.colisweb.util.Haversine
import fr.colisweb.util.model._

case class Delivery(collect: GeoPoint,
                    delivery: GeoPoint,
                    slot: TimeSlot,
                    packages: Seq[Package])  {

  lazy val totalWeight: WeightInKg = this.packages.map(_.weight).sum
  lazy val totalVolume: VolumeInCmCube = this.packages.map(_.volume).sum
  lazy val maxItemWeight: WeightInKg = this.packages.maxBy(_.weight).weight

  private def timeToDeliver(transporter: Transporter): DurationInMinutes = {
    val distance: DistanceInKm = Haversine.distanceInKm(collect, delivery)
    val durationInHours = distance / transporter.averageSpeed
    (durationInHours * 60).asInstanceOf[DurationInMinutes]
  }

  /** check if transporter can make the delivery within the slot time */
  def canMakeItOnTime(transporter: Transporter): Boolean = {
    val deliveryDuration: DurationInMinutes = timeToDeliver(transporter)
    deliveryDuration < slot.duration
  }

}
