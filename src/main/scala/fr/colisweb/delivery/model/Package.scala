package fr.colisweb.delivery.model

import fr.colisweb.util.model.{VolumeInCmCube, WeightInKg}

case class Package(weight: WeightInKg,
                   volume: VolumeInCmCube)
