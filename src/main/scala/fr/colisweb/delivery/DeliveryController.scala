package fr.colisweb.delivery

import cask.endpoints.JsonData
import cask.model.Status.{BadRequest, NotFound}
import cask.{Abort, Response}
import fr.colisweb.delivery.api.{DeliveryPayload, DeliveryResponse}
import fr.colisweb.util.api.{Controller, GetJsonPayload}

case class DeliveryController(deliveryService: DeliveryService = DeliveryService()) extends Controller {

  @GetJsonPayload("/delivery")
  def getDelivery(delivery: DeliveryPayload): Response[JsonData] = {
    val invalidFields = delivery.validate()
    if (invalidFields.isEmpty) {
      deliveryService.bestTransporter(delivery.toDelivery) match {
        case Some(transporter) => Response(DeliveryResponse(transporter))
        case None => Abort(NotFound.code)
      }
    } else {
      Response(invalidFields, BadRequest.code)
    }
  }

  initialize()
}